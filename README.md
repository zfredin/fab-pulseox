# FAB-pulseox

Fabricatable pulse oximeter for covid-19 response.

![pulseox_variety](img/pulseox_variety.jpg)

## status
Deprecated; moved to https://gitlab.cba.mit.edu/zfredin/public-health-instrumentation

## need
Blood oxygenation and heart rate, particularly, when combined with other metrics such as body temperature and respiration rate, may help give caregivers early notice of potential coronavirus infection. The need for a FabLab-built pulse-ox is less clear; the integrated sensors themselves seem to be in stock through the usual electronics distributors, and various online retailers appear to also have units ready for shipment. However, there is still value in providing an open reference design that uses an off-the-shelf sensor for integration into other projects. I'd also like to investigate building the sensor itself using generic optoelectronic components, but early research suggests this will be quite a bit more complicated due to the tight LED wavelength requirements.

## componentry
The first iteration of this device will be based on the Maxim MAX30101 integrated pulse oximetry sensor:

![max30101](img/max30101.jpg)

This chip is in stock; it's relatively cheap, at $8 in single-lot quantities; it's got built-in A/D and signal-processing capabilities, which takes care of most of the difficult parts of the measurement; and the LGA pads are SOIC-pitch (0.05" / 0.127 mm), so it shouldn't be too tough to solder (although LGAs aren't fun on milled PCBs).

The MAX30101 is an I2C device, so the reference circuit from the datasheet is quite simple:

![max30101_example](img/max30101_example.jpg)

For a first pass, this will get mashed together with appropriate voltage regulators (as the chip requires 5v and 1v8), a reasonable microcontroller like the SAMD11 or 21, and a UART connection for debugging. I'd like to focus on mechanical bits at the same time; my guess is the finger clip and associated hardware need to be well thought out to ensure accuracy. 
